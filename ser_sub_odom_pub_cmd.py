#!/usr/bin/env python
# from serial import Serial
import serial
import time

import rospy
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Twist

import sys
import signal

def signal_handler(signal, frame): # ctrl + c -> exit program
  print('You pressed Ctrl+C!')
  sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

def cmd_callback(msg):
  cmd = Twist()
  cmd.linear.x = msg.linear.x 
  cmd.linear.y = msg.linear.y 
  cmd.linear.z = msg.linear.z 
  cmd.angular.x = msg.angular.x 
  cmd.angular.y = msg.angular.y 
  cmd.angular.z = msg.angular.z 

  ser = serial.Serial('/dev/ttyUSB0', 19200, timeout=1)

  if ser.is_open:
    data = 's,%.2f,%.2f,%.2f,%.4f,%.4f,%.4f,'%(cmd.linear.x, cmd.linear.y, cmd.linear.z, cmd.angular.x, cmd.angular.y, cmd.angular.z)
    while len(data) < 80: 
      data = data + 'x'
    ser.write(data)
  else:
    ser = serial.Serial('/dev/ttyUSB0', 19200, timeout=1)

if __name__ == '__main__':
  ser = serial.Serial('/dev/ttyUSB0', 19200, timeout=1)  # open serial port
  rospy.init_node("serial_connector_2", anonymous=True)
  received_odom_pub = rospy.Publisher("/received_pose", Pose, queue_size=1)
  send_to_cmd_vel = rospy.Subscriber("/cmd_vel", Twist, cmd_callback)
  print(ser.name)         # check which port was really used

  while 1:
    try:
      if ser.is_open:
        d=ser.read(80)
        b=d.split(',')
        if b[0]!='s':
          ser.close()
        else:
          print(b)
          pmsg = Pose()
          pmsg.position.x = float(b[1])
          pmsg.position.y = float(b[2])
          pmsg.position.z = float(b[3])
          pmsg.orientation.x = float(b[4])
          pmsg.orientation.y = float(b[5])
          pmsg.orientation.z = float(b[6])
          pmsg.orientation.w = float(b[7])
          received_odom_pub.publish(pmsg)
      else:
        ser = serial.Serial('/dev/ttyUSB0', 19200, timeout=1)  # open serial port
    except (SystemExit, KeyboardInterrupt) :
      ser.close()             # close port
      sys.exit(0)

